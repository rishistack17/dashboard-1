import React, { Component } from 'react';
import {
View,
StyleSheet,
Text,
FlatList
} from "react-native";
import colors from "../themes/colors";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Info from "../components/IntoBox";
import TimeBox from "../components/TimeBox";
import data from "../data/data";
export default class HomePage extends Component {
    renderItem ({item}){
        return (
            <TimeBox month  = {item.month} number ={item.number} day={item.day}/>
        )
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <TimeBox month ="April" day ="sat" number = {12}/> */}
                <View style={styles.header}>
                    <Header/>
                </View>
                <View style= {styles.info}>
                    <Info/>
                </View>
                <View style={{
                    marginLeft:20,
                    marginBottom:20
                }}>
                    <View style={{
                        marginTop:20,
                        marginBottom:20
                    }}>
                        <Text style={{
                            color:"white",
                            fontWeight:"bold"
                        }}>
                            SELECT A DAY
                        </Text>
                    </View>
                    <FlatList
                    data ={data}
                    renderItem={this.renderItem}
                    keyExtractor={item =>item.id}
                    horizontal={true}
                    />
                </View>
                <View style = {styles.footer}>
                    <View style={{
                        flex:1,
                        alignItems:'flex-end'
                    }}>
                        <Footer/>
                    </View>
                    
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        backgroundColor:colors.background_home,
    },
    info:{
        marginTop:20,
        alignItems:"center"
    },
    header:{
        height:40,
        backgroundColor:"transparent"
    },
    footer:{
        flex:1,
        backgroundColor:"transparent"
    }
})