import React, { Component } from 'react'
import { View,Text, StyleSheet } from 'react-native'

export default class TimeBox extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text style={{
                        color:"black",
                        fontSize:10
                    }}>
                        {this.props.month}
                    </Text>
                    
                </View>
                <View>
                    <Text style={{
                        color:"black",
                        fontSize:20,
                        fontWeight:"bold"
                    }}>
                        {this.props.number}
                    </Text>
                </View>
                <View>
                    <Text>
                        {this.props.day}
                    </Text>
                </View>
            </View>
        )
    }
}

const styles  = StyleSheet.create({
    container:{
        display:'flex',
        width:60,
        height:70,
        backgroundColor:"white",
        borderRadius:10,
        justifyContent:'center',
        alignItems:"center",
        marginRight:10
    }
})
