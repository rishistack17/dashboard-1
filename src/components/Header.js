import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Dimensions,
    Text
} from "react-native";
import AntD from 'react-native-vector-icons/AntDesign';
import Feather from "react-native-vector-icons/Feather";
export default class Header extends Component {
    render() {
        return (
            <View style={styles.box}>
                <View style={styles.lefticon}>
                    <AntD name={"left"} size={20} style={{
                        color:"white"
                    }}/>
                </View>

                <View style={styles.heading}>
                    <Text style={{
                        color:"white"
                    }}>Create Schedule</Text> 
                </View>
               
               <View style={styles.bellicon}>
                   <Feather name={"bell"} size={20} style={{
                       color:"white"
                   }}/>
               </View>
               
            </View>
        )
    }
}
const width = Dimensions.get("screen").width
const styles =StyleSheet.create({
    box:{
        display:'flex',
        flexDirection:'row',
        width:width,
        backgroundColor:"transparent",
        height:40,
        alignItems:'center',
        justifyContent:'center'
    },
    bellicon:{
        flex:1,
        alignItems:'flex-end',
        marginRight:20
    },
    lefticon:{
        flex:1,
        marginLeft:20
    },
    heading:{
        flex:1,
        alignItems:'center'
    }
})