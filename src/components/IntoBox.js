import React, { Component } from 'react';
import { View, StyleSheet,Text, TouchableOpacity } from "react-native";
import dims from "../utils/dims";
import AntD from 'react-native-vector-icons/AntDesign';
import Ent from 'react-native-vector-icons/Entypo';
import colors from "../themes/colors";

export default class IntoBox extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.imagesection}>

                </View>
                <View style={styles.infosection}>
                    <View style={{
                        display:'flex',
                        flexDirection:"row",
                        
                    }}>
                        <View style={
                            styles.infointroname
                        }>
                            <Text style={{
                                color:"white"
                            }}>Sachin Dave</Text>
                        </View>
                        <View style={styles.infointrolocation}>
                            <Text style={{
                                color:"white"
                            }}> 
                            <Ent name={"location-pin"} size={15}/>
                                San fransisco
                            </Text>
                            
                        </View>
                    </View>
                    
                    <View style={{
                        display:"flex",
                        flexDirection:"row"
                        ,marginTop:10,
   
                    }}>
                        <View style={{
                            // flex:1,
                            display:"flex",
                            flexDirection:"row"
                        }}>
                        <AntD name={"star"} style={styles.star("yellow")} size={15}/>
                        <AntD name={"star"} style={styles.star("yellow")}size={15}/>
                        <AntD name={"star"} style={styles.star("yellow")}size={15}/>
                        <AntD name={"star"} style={styles.star("white")}size={15}/>
                        <AntD name={"star"} style={styles.star("white")}size={15}/>
                        </View>
                        <View style={{
                            alignItems:"flex-end"
                        }}>
                            <Text> 4.5</Text>
                        </View>
                        
                    </View>
                    <View style={{
                        display:"flex",
                        flexDirection:"row"
                    }}>
                        <View style={{
                            flex:1
                        }}>
                            <Text style={{
                                color:"white"
                            }}>
                                info section
                            </Text>
                            
                        </View>
                        <View style={{
                            flex:1,
                            alignItems:'center'
                        }}>
                            <TouchableOpacity style={{
                                backgroundColor:colors.buttoncolor,
                                width:90,
                                height:30,
                                borderRadius:10,
                                justifyContent:'center',
                                alignItems:'center',
                            }}>
                                <Text style={{
                                    color:"white"
                                }}>
                                    Book
                                </Text>
                            </TouchableOpacity>
                           
                            
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        display:'flex',
        flexDirection:'row',
        backgroundColor:"transparent",
        height:130,
        width:dims.width-30,
        borderWidth:1,
        borderColor:"white",
        borderRadius:10
    },
    imagesection:{
        flex:3/10,
        backgroundColor:"white",
        borderTopLeftRadius:10,
        borderBottomLeftRadius:10
    },
    infosection:{
        display:'flex',
        flexDirection:'column',
        flex:7/10,
        marginTop:10,
        marginLeft:10
    },
    infointroname:{
        flex:1,
        justifyContent:'center',
        alignItems:'flex-start'
    },
    infointrolocation:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        // marginRight:10
    },
    star(color){
        return{
            // flex:1,
            color:color
        }
        
    }
})