import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Touchable,
    TouchableOpacity
} from "react-native";
import colors from "../themes/colors";
export default class Header extends Component {
    render() {
        return (
            <View style={styles.box}>
                <View style={styles.money}>
                    <View style={styles.centrealign}>
                        <Text style={{
                            letterSpacing:2,
                            fontSize:28,
                            
                        }}>
                            $80
                        </Text>
                    </View>
                    <View style={styles.centrealign}>
                    <Text style={{
                        fontSize:22,
                        marginLeft:10
                    }}>TOTAL</Text> 
                    </View>
                    
                </View>


               
               <View style={styles.button}>
                   <TouchableOpacity style={styles.booknow}>
                       <Text style={{
                           fontSize:18
                       }}>
                           Book Now
                       </Text>
                   </TouchableOpacity>
               </View>
               
            </View>
        )
    }
}
const width = Dimensions.get("screen").width
const styles =StyleSheet.create({
    box:{
        
        display:'flex',
        flexDirection:'row',
        width:width,
        backgroundColor:"white",
        height:55,
        alignItems:'center',
        justifyContent:'center'
    },
    button:{
        flex:1,
        alignItems:'flex-end',
        marginRight:20
    },
    money:{
        flex:1,
        flexDirection:"row",
        marginLeft:20,
        // backgroundColor:"red",
        
    },

    booknow:{
        backgroundColor:colors.buttoncolor,
        alignItems:'center',
        justifyContent:'center',
        height:40,
        width:140,
        borderRadius:10
    },
    centrealign:{
        alignItems:'center',
        justifyContent:'center',
    }
})